package ru.t1.gorodtsova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.AbstractRequest;
import ru.t1.gorodtsova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
